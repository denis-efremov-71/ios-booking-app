//
//  UserStatusChecker.swift
//  Freightera
//
//  Created by Denis Efremov on 2019-02-14.
//  Copyright © 2019 Freightera. All rights reserved.
//

import SwiftKeychainWrapper

class UserStatusChecker {
    static func isUserLoggedIn() -> Bool {
        let accessToken: String? = KeychainWrapper.standard.string(forKey: "accessToken")
        
        if accessToken != nil {
            return true
        }
        else {
            return false
        }
    }
}

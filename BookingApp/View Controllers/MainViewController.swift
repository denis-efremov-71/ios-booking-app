//
//  MainViewController.swift
//  BookingApp
//
//  Created by Denis.Efremov on 2018-12-20.
//  Copyright © 2018 Freightera. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: class members
    
    var ads: [Ad] = []
    var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    var accessToken: String?
    var categorySelected: Int = 0
    
    // MARK: IBOutlets
    
    @IBOutlet var mainTableView: UITableView!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signOutButton: UIButton!
    
    // MARK: IBActions
    
    @IBAction func onQuoteButton(_ sender: Any) {
        performSegue(withIdentifier: "showWeb", sender: sender as! UIButton)
    }
    
    @IBAction func signIn(_ sender: Any) {
        goToLoginView()
    }
    
    @IBAction func signOut(_ sender: Any) {
        UiHelper.addActivityIndicator(view: view, activityIndicator: activityIndicator)
        accessToken = KeychainWrapper.standard.string(forKey: "accessToken")!
        
        if let token = accessToken {
            sendHttpRequest(token: token)
        }
        else {
            UiHelper.showAlert(for: self, with: "You have already signed out")
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showWeb" {
            let destVC = segue.destination as! WebViewController
            destVC.loadType = ((sender as! UIButton).titleLabel?.text)!
        }
        else if segue.identifier == "showBanners" {
            let destVC = segue.destination as! BannersViewController
            destVC.bannerCategory = categorySelected
        }
    }
    
    // MARK: UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ads.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "adCell", for: indexPath) as! CustomTableViewCell
        let i: Int = indexPath.row
        
        cell.adImageView.image = UIImage(named: ads[i].image)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Our Best Rates"
    }
    
    // MARK: UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        categorySelected = indexPath.row + 1
        performSegue(withIdentifier: "showBanners", sender: nil)
        //print("You selected row \(indexPath.row) in section \(indexPath.section)")
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.textLabel?.textColor = UIColor.black
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        header.textLabel?.frame = header.frame
    }
    
    // MARK: status bar
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: view controller's life cycle
    
    override func viewWillAppear(_ animated: Bool) {
        mainTableView.reloadData()
        if ads.count == 0 {
            ads.append(Ad(image: "ad1"))
            ads.append(Ad(image: "ad2"))
            ads.append(Ad(image: "ad3"))
        }
        
        // change the color of the status bar
        //UIApplication.shared.statusBarView?.backgroundColor = UIColor(red: CGFloat(11.0/255.0), green: CGFloat(126.0/255.0), blue: CGFloat(185.0/255.0), alpha: 1.0)
        
        headerImageView.image = UIImage(named: "header")
        
        if !UserStatusChecker.isUserLoggedIn() {
            buildGuestLayout()
        }
        else {
            buildSignedUserLayout()
        }
        
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: public methods
    
    func buildGuestLayout() {
        // Guest user - disable the Shipments and Account tabs and hide Sign Out button
        if  let arrayOfTabBarItems = self.tabBarController!.tabBar.items as AnyObject as? NSArray,
            let tabBarItem2 = arrayOfTabBarItems[1] as? UITabBarItem,
            let tabBarItem3 = arrayOfTabBarItems[2] as? UITabBarItem
        {
            tabBarItem2.isEnabled = false
            tabBarItem3.isEnabled = false
        }
        signInButton.isHidden = false
        signOutButton.isHidden = true
    }
    
    func buildSignedUserLayout() {
        if  let arrayOfTabBarItems = self.tabBarController!.tabBar.items as AnyObject as? NSArray
        {
            for tabBarItem in arrayOfTabBarItems {
                (tabBarItem as! UITabBarItem).isEnabled = true
            }
        }
        signInButton.isHidden = true
        signOutButton.isHidden = false
    }
    
    func goToLoginView() {
        let loginView = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let appDelegate = UIApplication.shared.delegate
        appDelegate?.window??.rootViewController = loginView
    }
    
    func sendHttpRequest(token: String) {
        
        let url = URL(string: Constants.LaravelApi.logout)
        var request = URLRequest(url: url!)
        
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("XMLHttpRequest", forHTTPHeaderField: "X-Requested-With")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            UiHelper.removeActivityIndicator(activityIndicator: self.activityIndicator)
            
            if error != nil {
                UiHelper.showAlert(for: self, with: "HTTP sign out request failed with error: \(String(describing: error))")
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: [.allowFragments]) as? NSDictionary
                
                if let parseJSON = json {
                    
                    // wrong email or password entered
                    let error = parseJSON["error"] as? String
                    
                    if let errorResponse = error {
                        UiHelper.showAlert(for: self, with: "Error: \(errorResponse )")
                        return
                    }
                    
                    let message = parseJSON["message"] as? String
                    
                    if (message?.isEmpty)! {
                        UiHelper.showAlert(for: self, with: "Error: no message received from server")
                        return
                    }
                    else {
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertController.Style.alert)
                            let alertAction = UIAlertAction(title: "OK", style: .default, handler: {(action: UIAlertAction) in
                                self.continueAfterAlertDismiss()
                            })
                            alertController.addAction(alertAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                }
                else {
                    UiHelper.showAlert(for: self, with: "HTTP sign out request returned no data")
                    return
                }
            }
            catch let error {
                UiHelper.removeActivityIndicator(activityIndicator: self.activityIndicator)
                UiHelper.showAlert(for: self, with: "JSONSerialization faled with error: \(error)")
                return
            }
        }
        
        task.resume()
    }
    
    func continueAfterAlertDismiss() {
        let _: Bool = KeychainWrapper.standard.removeObject(forKey: "accessToken")
        let _: Bool = KeychainWrapper.standard.removeObject(forKey: "userEmail")
        
        DispatchQueue.main.async {
            self.goToLoginView()
        }
    }
}

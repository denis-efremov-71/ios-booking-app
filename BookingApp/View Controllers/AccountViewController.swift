//
//  AccountViewController.swift
//  BookingApp
//
//  Created by Denis.Efremov on 2018-12-26.
//  Copyright © 2018 Freightera. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {

    // MARK: outlets
    
    @IBOutlet weak var headerImageView: UIImageView!
    
    // MARK: actions
    
    @IBAction func editProfile(_ sender: Any) {
        performSegue(withIdentifier: "showEditProfile", sender: sender as! UIButton)
    }
    
    // MARK: status bar
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: view controller's life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        headerImageView.image = UIImage(named: "header")
        super.viewWillAppear(animated)
    }

}

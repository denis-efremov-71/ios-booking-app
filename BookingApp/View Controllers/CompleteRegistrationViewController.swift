//
//  CompleteRegistrationViewController.swift
//  Freightera
//
//  Created by Denis Efremov on 2019-02-11.
//  Copyright © 2019 Freightera. All rights reserved.
//

import UIKit

class CompleteRegistrationViewController: UIViewController {

    // MARK: outlets
    
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var addressOneTextField: UITextField!
    @IBOutlet weak var addressTwoTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var zipTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var currencyTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var retypePasswordTextField: UITextField!
    @IBOutlet weak var understandSwitch: UISwitch!
    @IBOutlet weak var acceptSwitch: UISwitch!
    
    // MARK: actions
    
    @IBAction func submit(_ sender: Any) {
        if !validate() || !comparePasswords() || !checkUsersConsent() {
            return
        }
        // all the validations and checks are passed
        else {
            let thankYouViewController = self.storyboard?.instantiateViewController(withIdentifier: "ThankYouViewController") as! ThankYouViewController
            
            self.present(thankYouViewController, animated: true)
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: status bar
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: view controller's life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        headerImageView.image = UIImage(named: "header")
        
        super.viewWillAppear(animated)
    }

    // MARK: public methods
    
    func validate() -> Bool {
        do {
            _ = try self.addressOneTextField.validatedText(validationType: ValidatorType.requiredField(field: "Address 1"))
            _ = try self.cityTextField.validatedText(validationType: ValidatorType.requiredField(field: "City"))
            _ = try self.zipTextField.validatedText(validationType: ValidatorType.requiredField(field: "ZIP/Postal Code"))
            _ = try self.stateTextField.validatedText(validationType: ValidatorType.requiredField(field: "State/Province"))
            _ = try self.currencyTextField.validatedText(validationType: ValidatorType.requiredField(field: "Currency"))
            _ = try self.passwordTextField.validatedText(validationType: ValidatorType.password)
            _ = try self.retypePasswordTextField.validatedText(validationType: ValidatorType.retypedPassword)
            return true
        } catch(let error) {
            UiHelper.showAlert(for: self, with: (error as! ValidationError).message)
            return false
        }
    }
    
    func comparePasswords() -> Bool {
        if (passwordTextField.text?.elementsEqual(retypePasswordTextField.text!))! {
            return true
        }
        else {
            UiHelper.showAlert(for: self, with: "Password and retyped password do not match")
            return false
        }
    }
    
    func checkUsersConsent() -> Bool {
        if understandSwitch.isOn && acceptSwitch.isOn {
            return true
        }
        else {
            UiHelper.showAlert(for: self, with: "Please give us your consent to receive emails and accept our Terms and Conditions")
            return false
        }
    }

}

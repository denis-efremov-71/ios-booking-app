//
//  BannersViewController.swift
//  Freightera
//
//  Created by Denis Efremov on 2019-02-19.
//  Copyright © 2019 Freightera. All rights reserved.
//

import UIKit

class BannersViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // MARK: class members
    
    var bannerCategory: Int = 1
    var banners: [Banner] = []
    
    // MARK: outlets
    
    @IBOutlet weak var bannersTableView: UITableView!
    @IBOutlet weak var headerImageView: UIImageView!
    
    // MARK: actions
    
    @IBAction func onBackButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let i = ((sender as! UITableView).indexPathForSelectedRow?.row)!
        let quoteHash = banners[i].hash
        let destVC = segue.destination as! WebViewBannerRate
        destVC.quoteHash = quoteHash
    }
    
    // MARK: UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return banners.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bannerCell", for: indexPath) as! BannerTableViewCell
        let i: Int = indexPath.row

        cell.bannerImageView.image = UIImage(named: banners[i].image)
        
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var header: String = ""

        switch bannerCategory {
        case 1:
            header = "Full Truck Load Rates"
            break
        case 2:
            header = "Pallet Shipiping Rates"
            break
        case 3:
            header = "FTL by Rail"
            break
        default:
            break
        }
        return header.uppercased()
    }
    
    // MARK: UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 172.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("You selected row \(indexPath.row) in section \(indexPath.section)")
        performSegue(withIdentifier: "showWebBannerRate", sender: tableView)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.layoutMargins.bottom = 10
        header.textLabel?.textColor = UIColor.black
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        header.textLabel?.frame = header.frame
    }
    
    // MARK: status bar
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: view controller's life cycle
    
    override func viewWillAppear(_ animated: Bool) {
        bannersTableView.reloadData()
        if banners.count == 0 {
            switch self.bannerCategory {
            case 1:
                banners.append(Banner(category: bannerCategory, hash: "a7a195198cf40237ce57640ad9a6c5ed", image: "ftl_truck_1")) // denis
                //banners.append(Banner(category: bannerCategory, hash: "", image: "ftl_truck_1")) // live
                banners.append(Banner(category: bannerCategory, hash: "e0e6ea66dde2cc40dcafc8525abb6a4a", image: "ftl_truck_2")) // denis
                //banners.append(Banner(category: bannerCategory, hash: "", image: "ftl_truck_2")) // live
                banners.append(Banner(category: bannerCategory, hash: "6d330495ffd8cf31b9ab6bc483bc4263", image: "ftl_truck_3")) // denis
                //banners.append(Banner(category: bannerCategory, hash: "", image: "ftl_truck_3")) // live
                banners.append(Banner(category: bannerCategory, hash: "ee25d288d755b74067ecaa4b7709e001", image: "ftl_truck_4")) // denis
                //banners.append(Banner(category: bannerCategory, hash: "", image: "ftl_truck_4")) // live
                banners.append(Banner(category: bannerCategory, hash: "8a75f0cd09f37431f5cdd642150df9c0", image: "ftl_truck_5")) // denis
                //banners.append(Banner(category: bannerCategory, hash: "", image: "ftl_truck_5")) // live
                break
            case 2:
                banners.append(Banner(category: bannerCategory, hash: "e807a31ef3d6e46ed1666d981ad5d6b7", image: "pallet_1")) // denis
                //banners.append(Banner(category: bannerCategory, hash: "", image: "pallet_1")) // live
                banners.append(Banner(category: bannerCategory, hash: "e9792d11d8ad931692961ed227f2c276", image: "pallet_2")) // denis
                //banners.append(Banner(category: bannerCategory, hash: "", image: "pallet_2")) // live
                banners.append(Banner(category: bannerCategory, hash: "e5231f6949ad35e1d348e5526387dad2", image: "pallet_3")) // denis
                //banners.append(Banner(category: bannerCategory, hash: "", image: "pallet_3")) // live
                banners.append(Banner(category: bannerCategory, hash: "dd9439252122d670301f80ee7ecd579c", image: "pallet_4")) // denis
                //banners.append(Banner(category: bannerCategory, hash: "", image: "pallet_4")) // live
                banners.append(Banner(category: bannerCategory, hash: "de3ff1e7cb4fbccf25d3329e7a8c4ba8", image: "pallet_5")) // denis
                //banners.append(Banner(category: bannerCategory, hash: "", image: "pallet_5")) // live
                break
            case 3:
                banners.append(Banner(category: bannerCategory, hash: "4c3e609f3ec828e0ad1dd4f8bcc93f97", image: "ftl_rail_1")) // denis
                //banners.append(Banner(category: bannerCategory, hash: "", image: "ftl_rail_1")) // live
                banners.append(Banner(category: bannerCategory, hash: "062e11dbca0c8fdb41911882543d86b8", image: "ftl_rail_2")) // denis
                //banners.append(Banner(category: bannerCategory, hash: "", image: "ftl_rail_2")) // live
                banners.append(Banner(category: bannerCategory, hash: "ab3c9175b754eda3002e2818f03e1955", image: "ftl_rail_3")) // denis
                //banners.append(Banner(category: bannerCategory, hash: "", image: "ftl_rail_3")) // live
                banners.append(Banner(category: bannerCategory, hash: "040b1d33cc6b61953ff137d18b3dc3ee", image: "ftl_rail_4")) // denis
                //banners.append(Banner(category: bannerCategory, hash: "", image: "ftl_rail_4")) // live
                break
            default:
                break
            }
        }
        
        headerImageView.image = UIImage(named: "header")
        
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    


}

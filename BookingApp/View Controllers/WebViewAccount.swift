//
//  WebViewAccount.swift
//  BookingApp
//
//  Created by Denis Efremov on 2019-01-09.
//  Copyright © 2019 Freightera. All rights reserved.
//

import UIKit
import WebKit
import SwiftKeychainWrapper

class WebViewAccount: UIViewController, WKNavigationDelegate {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var webView: WKWebView!
    
    // MARK: IBActions
    
    @IBAction func onDoneButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: class members
    
    var accessToken: String?
    let baseUrl: String = "https://\(Constants.siteDomainDev).freightera.com/en/freight-shippers/edit-profile"
    
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView()
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.color = .black
        return spinner
    }()
    
    // MARK: WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.view.addSubview(self.loadingIndicator)
        self.loadingIndicator.startAnimating()
        NSLayoutConstraint.activate([self.loadingIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
                                     self.loadingIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)])
        self.view.bringSubviewToFront(self.loadingIndicator)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.loadingIndicator.stopAnimating()
        self.loadingIndicator.removeFromSuperview()
        
        // remove unwanted Web contents
        webView.removeWebContents()
        
        webView.isHidden = false
    }
    
    // MARK: view life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        webView.navigationDelegate = self
        
        accessToken = KeychainWrapper.standard.string(forKey: "accessToken")!
        
        // load Web page
        var request = URLRequest(url: URL(string: baseUrl)!)
        //request.addValue("Authorization", forHTTPHeaderField: "Bearer \(accessToken!)")   // TODO: uncomment once FERA-2699 completed
        request.addValue("Denis", forHTTPHeaderField: "JWT-Auth")
        webView.load(request)
        webView.isHidden = true
    }
}

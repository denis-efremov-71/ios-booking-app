//
//  LoginViewController.swift
//  Freightera
//
//  Created by Denis Efremov on 2019-02-11.
//  Copyright © 2019 Freightera. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class LoginViewController: UIViewController {
    
    var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)

    // MARK: outlets
    
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    // MARK: actions
    
    @IBAction func signIn(_ sender: Any) {
        if !validate() {
            return
        }
        
        UiHelper.addActivityIndicator(view: view, activityIndicator: activityIndicator)
        sendHttpRequest()
    }
    
    @IBAction func continueAsGuest(_ sender: Any) {
        goToHomeView()
    }
    
    @IBAction func registerNewAccount(_ sender: Any) {
        let registerViewController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        
        self.present(registerViewController, animated: true)
    }
    
    // MARK: status bar
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: view controller's life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        headerImageView.image = UIImage(named: "header")
        
        super.viewWillAppear(animated)
    }

    // MARK: public methods
    
    func validate() -> Bool {
        do {
            _ = try self.emailTextField.validatedText(validationType: ValidatorType.email)
            _ = try self.passwordTextField.validatedText(validationType: ValidatorType.password)
            return true
        } catch(let error) {
            UiHelper.showAlert(for: self, with: (error as! ValidationError).message)
            return false
        }
    }
    
    func goToHomeView() {
        let tabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
        
        let appDelegate = UIApplication.shared.delegate
        appDelegate?.window??.rootViewController = tabBarController
    }
    
    func sendHttpRequest() {
        let url = URL(string: Constants.LaravelApi.login)
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("XMLHttpRequest", forHTTPHeaderField: "X-Requested-With")
        
        let userEmail = emailTextField.text
        let userPassword = passwordTextField.text
        
        let postString = ["email": userEmail!, "password": userPassword!] as [String: String]
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: postString)
        }
        catch let error {
            UiHelper.showAlert(for: self, with: "JSONSerialization failed with error: \(error.localizedDescription)")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            UiHelper.removeActivityIndicator(activityIndicator: self.activityIndicator)
            
            if error != nil {
                UiHelper.showAlert(for: self, with: "HTTP login request failed with error: \(String(describing: error))")
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: [.allowFragments]) as? NSDictionary
                
                if let parseJSON = json {
                    
                    // wrong email or password entered
                    let error = parseJSON["error"] as? String
                    
                    if var errorResponse = error {
                        if errorResponse == "Unauthorised" {
                            errorResponse = "Wrong email or password entered"
                        }
                        UiHelper.showAlert(for: self, with: "Error: \(errorResponse )")
                        return
                    }
                    
                    // email and password were both correct
                    let accessToken = parseJSON["access_token"] as? String
                    
                    if (accessToken?.isEmpty)! {
                        UiHelper.showAlert(for: self, with: "Error: empty access token returned")
                        return
                    }
                    
                    let _: Bool = KeychainWrapper.standard.set(accessToken!, forKey: "accessToken")
                    let _: Bool = KeychainWrapper.standard.set(userEmail!, forKey: "userEmail")
                    
                    DispatchQueue.main.async {
                        self.goToHomeView()
                    }
                }
                else {
                    UiHelper.showAlert(for: self, with: "HTTP login request returned no data")
                    return
                }
            }
            catch let error {
                UiHelper.removeActivityIndicator(activityIndicator: self.activityIndicator)
                UiHelper.showAlert(for: self, with: "JSONSerialization faled with error: \(error)")
                return
            }
        }
        
        task.resume()
    }

}

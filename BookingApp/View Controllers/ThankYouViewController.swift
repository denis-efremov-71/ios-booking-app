//
//  ThankYouViewController.swift
//  Freightera
//
//  Created by Denis Efremov on 2019-02-12.
//  Copyright © 2019 Freightera. All rights reserved.
//

import UIKit

class ThankYouViewController: UIViewController {

    // MARK: outlets
    
    @IBOutlet weak var headerImageView: UIImageView!
    
    // MARK: actions
    
    @IBAction func goToLogin(_ sender: Any) {
        let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        self.present(loginViewController, animated: true)
    }
    
    @IBAction func continueAsGuest(_ sender: Any) {
        let tabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
        
        self.present(tabBarController, animated: false, completion: nil)
    }
    
    // MARK: status bar
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: view controller's life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        headerImageView.image = UIImage(named: "header")
        
        super.viewWillAppear(animated)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  WebViewController.swift
//  BookingApp
//
//  Created by Denis.Efremov on 2018-12-20.
//  Copyright © 2018 Freightera. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {

    // MARK: IBOutlets
    
    @IBOutlet weak var webView: WKWebView!
    
    // MARK: IBActions
    
    @IBAction func onDoneButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: class members
    
    var loadType: String = "LTL"
    var baseUrl: String = "https://www.freightera.com/en/freight-shippers/quote?lane_type="
    
    private var loadingObservation: NSKeyValueObservation?
    
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView()
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.color = .black
        return spinner
    }()
    
    // MARK: WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.view.addSubview(self.loadingIndicator)
        self.loadingIndicator.startAnimating()
        NSLayoutConstraint.activate([self.loadingIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
                                     self.loadingIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)])
        self.view.bringSubviewToFront(self.loadingIndicator)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.loadingIndicator.stopAnimating()
        self.loadingIndicator.removeFromSuperview()
        
        // remove unwanted Web contents
        webView.removeWebContents()
        webView.isHidden = false
    }
    
    // MARK: view life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.navigationDelegate = self

        // load Web page
        let request = URLRequest(url: URL(string: baseUrl + loadType)!)
        webView.load(request)
        webView.isHidden = true
    }
}

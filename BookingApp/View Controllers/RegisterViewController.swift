//
//  RegisterViewController.swift
//  Freightera
//
//  Created by Denis Efremov on 2019-02-11.
//  Copyright © 2019 Freightera. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    // MARK: outlets
    
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var companyNameTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var businessEmailTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    // MARK: actions
    
    @IBAction func `continue`(_ sender: Any) {
        if (!validate()) {
            return
        }
        let completeRegistrationViewController = self.storyboard?.instantiateViewController(withIdentifier: "CompleteRegistrationViewController") as! CompleteRegistrationViewController
        
        self.present(completeRegistrationViewController, animated: true)
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: status bar
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: view controller's life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        headerImageView.image = UIImage(named: "header")
        
        super.viewWillAppear(animated)
    }

    // MARK: public methods
    
    func validate() -> Bool {
        do {
            _ = try self.firstNameTextField.validatedText(validationType: ValidatorType.requiredField(field: "First Name"))
            _ = try self.lastNameTextField.validatedText(validationType: ValidatorType.requiredField(field: "Last Name"))
            _ = try self.companyNameTextField.validatedText(validationType: ValidatorType.requiredField(field: "Company Name"))
            _ = try self.countryTextField.validatedText(validationType: ValidatorType.requiredField(field: "Country"))
            _ = try self.businessEmailTextField.validatedText(validationType: ValidatorType.email)
            _ = try self.phoneNumberTextField.validatedText(validationType: ValidatorType.requiredField(field: "Phone Number"))
            return true
        } catch(let error) {
            UiHelper.showAlert(for: self, with: (error as! ValidationError).message)
            return false
        }
    }

}

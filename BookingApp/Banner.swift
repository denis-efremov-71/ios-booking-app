//
//  Banner.swift
//  Freightera
//
//  Created by Denis Efremov on 2019-02-20.
//  Copyright © 2019 Freightera. All rights reserved.
//

import Foundation

class Banner {
    var category: Int = 1
    var hash: String = ""
    var image: String = ""
    
    init(category: Int, hash: String, image: String) {
        self.category = category
        self.hash = hash
        self.image = image
    }
}

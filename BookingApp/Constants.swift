//
//  Constants.swift
//  Freightera
//
//  Created by Denis Efremov on 2019-02-13.
//  Copyright © 2019 Freightera. All rights reserved.
//

struct Constants {
    
    static let laravelApiDomain = "denis.api"
    static let siteDomainDev = "denis"
    static let siteDomainLive = "www"
    
    // Laravel API end-points
    struct LaravelApi {
        static let login = "http://\(laravelApiDomain).freightera.com/v1/shippers/login"
        static let logout = "http://\(laravelApiDomain).freightera.com/v1/shippers/logout2"
        static let getUserProfile = "http://\(laravelApiDomain).freightera.com/v1/shippers/profile2"
    }
    
    // Web site URLs
    struct FreighteraUrl {
        static let myActiveShipments = "https://\(siteDomainDev).freightera.com/en/freight-shippers/all-my-shipments"
    }
    
}

//
//  Ad.swift
//  BookingApp
//
//  Created by Denis.Efremov on 2018-12-21.
//  Copyright © 2018 Freightera. All rights reserved.
//

import Foundation

class Ad {
    var image: String = ""
    
    init(image: String) {
        self.image = image
    }
}

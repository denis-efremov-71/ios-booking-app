//
//  AppExtensions.swift
//  BookingApp
//
//  Created by Denis.Efremov on 2018-12-26.
//  Copyright © 2018 Freightera. All rights reserved.
//

import UIKit

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector("statusBar")) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}

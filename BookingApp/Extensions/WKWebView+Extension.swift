//
//  WKWebView+Extension.swift
//  Freightera
//
//  Created by Denis Efremov on 2019-01-18.
//  Copyright © 2019 Freightera. All rights reserved.
//

import Foundation
import WebKit

extension WKWebView {
    func removeWebContents() {
        let jsScript = "$('.menu-button').remove();$('#chat-widget-container').remove();"
        self.evaluateJavaScript(jsScript, completionHandler: nil)
    }
    
    func getShipmentInfo(trackingNumber: String) {
        let jsScript = "$('#trackingID').val('\(trackingNumber)');$('.trackingIDButton').click();"
        self.evaluateJavaScript(jsScript, completionHandler: nil)
    }
    
    func getAccountInfo(login: String, password: String) {
        let jsScript = "$('#login_signup_id').click();$('#LoginFormEmail').val('\(login)');$('#LoginFormPassword').val('\(password)');$('.signin-modal-btn').click();"
        self.evaluateJavaScript(jsScript, completionHandler: nil)
    }
}

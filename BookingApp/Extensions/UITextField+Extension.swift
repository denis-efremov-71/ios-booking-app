//
//  UITextField+Extension.swift
//  BookingApp
//
//  Created by Denis Efremov on 2019-01-03.
//  Copyright © 2019 Freightera. All rights reserved.
//

import UIKit.UITextField

extension UITextField {
    func validatedText(validationType: ValidatorType) throws -> String {
        let validator = VaildatorFactory.validatorFor(type: validationType)
        return try validator.validated(self.text!)
    }
}
